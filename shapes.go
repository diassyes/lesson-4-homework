package main

import (
	"fmt"
	"math"
)

// Interface for figures
type GeometricFigure interface{
	Area() float64
	Name() string
}

// Prints information about figure area
func printArea(figure GeometricFigure){
	fmt.Printf("%s area is %.1f\n", figure.Name(), figure.Area())
}

// -------------- Square --------------
// Initialized a Square struct:
// 		A - length of the side
type Square struct{
	A float64
}

// Returns the figure name
func (square *Square) Name() string {
	return "Square"
}

// Returns the figure area
func (square *Square) Area() float64{
	return math.Pow(square.A, 2)
}

// -------------- Rectangle --------------
// Initialized a Rectangle struct:
// 		A - length of the first side
// 		B - length of the second side
type Rectangle struct{
	A float64
	B float64
}

// Returns the figure name
func (rectangle *Rectangle) Name() string {
	return "Rectangle"
}

// Returns the figure area
func (rectangle *Rectangle) Area() float64{
	return rectangle.A*rectangle.B
}

// -------------- Circle --------------
// Initialized a Rectangle struct:
// 		Radius - radius of the circle
type Circle struct{
	Radius float64
}

// Returns the figure name
func (circle *Circle) Name() string {
	return "Circle"
}

// Returns the figure area
func (circle *Circle) Area() float64{
	return math.Pi*math.Pow(circle.Radius, 2)
}

func main(){
	square := Square{5}
	printArea(&square)

	rectangle := Rectangle{7, 8}
	printArea(&rectangle)

	circle := Circle{12.5}
	printArea(&circle)

	// areaSquare := square.Area() - We can save the area value in the variable
	// areaRectangle := rectangle.Area() - We can save the area value in the variable
	// areaCircle := circle.Area() - We can save the area value in the variable
}